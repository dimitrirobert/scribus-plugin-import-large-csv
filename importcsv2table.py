#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ABOUT THIS SCRIPT:

Import CSV data files as tables into Scribus

1. Create any frame of any size on your page but positioned 
where you want the table to be located (upper left corner)

2. Make sure it is selected

3. Execute this script:

You will be prompted first for the width of the left column in mm,
then the right column in mm, then height of all cells, and finally
for a csv filename

4. The data from the csv file will be imported and a table of
textboxes will be drawn on the page.

LIMITATIONS:

1. You are limited to two-column CSV data in your file.

2. In Scribus versions 1.3.5svn, when the script ends, you cannot
adjust text, colors, and line features for a group, whereas in 1.3.3.x,
all of these can be done without ungrouping.

HINTS:

Postgresql:
You can easily create a CSV file with a Postgresql database. From Postgresql,
toggle unaligned output with the '\a' switch, then activate a comma as
a separator with '\f ,' (without apostrophes). Send output to a file
with '\o myfile.csv', then query your database.

Sqlite3:
You can use "sqlite3 -csv" in the command line or ".mode csv" in sqlite's
interactive shell.

############################

LICENSE:

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

Author: Sebastian Stetter

Modifications: Gregory Pittman

please report bugs to: scribusscript@sebastianstetter.de
"""

from __future__ import division
import sys

try:
    # Please do not use 'from scribus import *' . If you must use a 'from import',
    # Do so _after_ the 'import scribus' and only import the names you need, such
    # as commonly used constants.
    import scribus
except ImportError,err:
    print "This Python script is written for the Scribus scripting interface."
    print "It can only be run from within Scribus."
    sys.exit(1)

#########################
# YOUR IMPORTS GO HERE  #
#########################
import csv

# get informations from selected frame
def getFrameData():
    if scribus.selectionCount() == 1:
        frameData = dict()
        areaname = scribus.getSelectedObject()
        position = scribus.getPosition(areaname)
        size = scribus.getSize(areaname)
        frameData['x'] = position[0]
        frameData['y'] = position[1]
        frameData['w'] = size[0]
        frameData['h'] = size[1]
        frameData['lineColor'] = scribus.getLineColor()
        frameData['lineWidth'] = scribus.getLineWidth()

        if scribus.getObjectType() == "TextFrame":
            td = scribus.getTextDistances()
            frameData['padL'],frameData['padR'],frameData['padT'],frameData['padB'] = td
        else:
            frameData['padL'],frameData['padR'],frameData['padT'],frameData['padB'] = (0,0,0,0)

        return frameData
        
    else: 
        scribus.messageBox("csv2table", "Merci de sélectionner un cadre de texte marquant la zone occupée par le tableau.\nSes propriétés (style, distances du texte, filet) seront utilisées pour les cellules.")
        sys.exit()

##get information about the area where the bale should be drawed
#def getPosition():
#    if scribus.selectionCount() == 1:
#        areaname = scribus.getSelectedObject()
#        position= scribus.getPosition(areaname)
#        vpos = position[1]
#        hpos = position[0]
#        return vpos, hpos
#        
#    else: 
#        scribus.messageBox("csv2table", "please select ONE Object to mark the drawing area for the table")
#        sys.exit()
#
##get information about the area where the bale should be drawed
#def getTableSize():
#    if scribus.selectionCount() == 1:
#        areaname = scribus.getSelectedObject()
#        return scribus.getSize(areaname)
#    else: 
#        scribus.messageBox("csv2table", "please select ONE Object to mark the drawing area for the table")
#        sys.exit()
#
#def getTextDistances():
#    if scribus.selectionCount() == 1:
#        if scribus.getObjectType() == "TextFrame":
#            return scribus.getTextDistances()
#        else:
#            return (0,0,0,0)
#    else: 
#        scribus.messageBox("csv2table", "please select ONE Object to mark the drawing area for the table")
#        sys.exit()

#get the cvs data
def getCSVdata():
    """opens a csv file, reads it in and returns a 2 dimensional list with the data"""
    csvfile = scribus.fileDialog("csv2table :: open file", "*.csv")
    csvDelimiter = scribus.valueDialog('Délimiteur','Délimiteur de colonnes dans le CSV',';')
    if csvfile != "":
        try:
            reader = csv.reader(file(csvfile), delimiter=csvDelimiter)
            datalist=[]
            for row in reader:
                rowlist=[]
                for col in row:
                    rowlist.append(col)
                datalist.append(rowlist)
            return datalist
        except Exception,  e:
            scribus.messageBox("csv2table", "Could not open file %s"%e)
    else:
        sys.exit

def getDataInformation(list):
    """takes a 2 dimensional list object and returns the numbers of rows and cols"""
    datainfo = dict()
    datainfo["rowcount"]=len(list)
    datainfo["colcount"]= len(list[0])
    return datainfo

#def cellsize(areainfo, datainfo):
#    """"takes the area and data info and calculates the prper cell size"""
#    csize=dict()
#    csize["v"]= areainfo["vsize"] / datainfo["rowcount"]
#    csize["h"]= areainfo["hsize"] / datainfo["colcount"]
#    return csize

# Boîte de dialogue pour demander le style de paragraphe des cellules
# S'il n'existe pas, on le crée
def getStyle():
    listStyles = scribus.getAllStyles()

    style = scribus.valueDialog("Style des cellules", "Indiquez le style de paragraphe pour le contenu des cellules.\nStyles existants :\n" + '\n'.join(listStyles), "cellule")
    if style == "":
        style = "cellule"
    if style not in listStyles:
        scribus.createParagraphStyle(style)
    return style
    
def main(argv):
    """This is a documentation string. Write a description of what your code
    does here. You should generally put documentation strings ("docstrings")
    on all your Python functions."""
    #########################
    #  YOUR CODE GOES HERE  #
    #########################
    if not scribus.haveDoc() > 0: #do we have a doc?
        scribus.messageBox("importcvs2table", "No opened document.\nPlease open one first.")
        sys.exit()
    userdim = scribus.getUnit() #get unit and change it to mm
    scribus.setUnit(scribus.UNIT_MILLIMETERS)
    cellHeight = 0
    fd = getFrameData()

    data = getCSVdata()
    di = getDataInformation(data)
    tableX = cellX = fd['x']
    tableY = cellY = fd['y']
    tableW = fd['w']
    tableH = fd['h']

    columns = "1 "
    columns *= di['colcount'] - 1
    columns += "1"
    ok = 0
    while ok == 0:
        columns = scribus.valueDialog('Largeur des colonnes','Indiquez la part de chaque colonne suivant le modèle ci-dessous (toutes les colonnes identiques).\nVotre tableau a '+str(di['colcount'])+' colonnes et '+str(di['rowcount'])+' lignes', columns)
        columnsPart = columns.split()
        columnsPart = map(int, columnsPart)
        partSum = sum(columnsPart)
        if len(columnsPart) == di['colcount']:
            ok = 1

    cellWidth = fd['w'] / partSum

    isTitleLine = scribus.valueDialog('Ligne de titre', 'La première ligne est-elle une ligne de titre (o/n) ?', 'o')
    if isTitleLine in "oOyY":
        isTitleLine = True
    else:
        isTitleLine = False

    while cellHeight <= 0:
        cellheight = scribus.valueDialog('Hauteur des cellules','Quelle hauteur pour les cellules en mm ?','10.0')
        if (not cellheight) :
            sys.exit()
        cellHeight = float(cellheight)

    maxLinePerPage = tableH / cellHeight
    maxLinePerPage = int(maxLinePerPage)

    if isTitleLine:
        maxLinePerPage -= 1

    # calcul du décalage à appliquer entre les morceaux de tableaux sur plusieurs pages
    marginT, marginL, marginR, marginB = scribus.getPageNMargins(scribus.currentPage())
    offsetTab = marginT + marginB + cellHeight + 14.111 # espace vertical entre les pages

    style = getStyle()
    
    # suppression de l'objet qui nous sert de support
    scribus.deleteObject(scribus.getSelectedObject())

    objectlist=[] # here we keep a record of all the created textboxes so we can group them later
    i = lpp = 0
    scribus.progressTotal(len(data))
    scribus.setRedraw(False)
    for row in data:
        c=0
        rowlist=[]
        for cell in row:
            cell = cell.strip()
            thisCellWidth = cellWidth * columnsPart[c]
            textbox = scribus.createText(cellX, cellY, thisCellWidth, cellHeight) #create a textbox
            rowlist.append(textbox)

            scribus.insertText(cell,0, textbox)#insert the text into the textbox
            scribus.setTextDistances(fd['padL'], fd['padR'], fd['padT'], fd['padB'], textbox)
            scribus.setStyle(style, textbox)
            scribus.setLineColor(fd['lineColor'], textbox)
            scribus.setLineWidth(fd['lineWidth'], textbox)

            cellX = cellX + thisCellWidth #move the position for the next cell
            c+=1
        cellY = cellY + cellHeight #set vertical position for next row
        cellX = tableX #reset horizontal position for next row

        # recherche du nom du groupe qu'on va créer là
        #objectListBefore = scribus.getAllObjects()
        scribus.groupObjects(rowlist)
        if i != 0:
            lpp += 1
        
        if lpp == maxLinePerPage:
            lpp = 0
            cellY += offsetTab
        #objectListAfter = scribus.getAllObjects()
        #if i == 1:
        #    scribus.messageBox("test", objectListAfter[-di['colcount']-1])
        #    scribus.messageBox("Liste", str(objectListAfter))
        #groupName = set(objectListAfter) - set(objectListBefore)
        #groupName = groupName.pop()
        #if scribus.objectExists(groupName):

        #objectlist.append(objectListAfter[-di['colcount']-1])

        i=i+1
        scribus.progressSet(i)
    
    #scribus.groupObjects(objectlist)
    scribus.progressReset()
    scribus.setUnit(userdim) # reset unit to previous value
    scribus.docChanged(True)
    scribus.statusMessage("Done")
    scribus.setRedraw(True)

def main_wrapper(argv):
    """The main_wrapper() function disables redrawing, sets a sensible generic
    status bar message, and optionally sets up the progress bar. It then runs
    the main() function. Once everything finishes it cleans up after the main()
    function, making sure everything is sane before the script terminates."""
    try:
        scribus.statusMessage("Importing .csv table...")
        scribus.progressReset()
        main(argv)
    finally:
        # Exit neatly even if the script terminated with an exception,
        # so we leave the progress bar and status bar blank and make sure
        # drawing is enabled.
        if scribus.haveDoc() > 0:
            scribus.setRedraw(True)
        scribus.statusMessage("")
        scribus.progressReset()

# This code detects if the script is being run as a script, or imported as a module.
# It only runs main() if being run as a script. This permits you to import your script
# and control it manually for debugging.
if __name__ == '__main__':
    main_wrapper(sys.argv)
