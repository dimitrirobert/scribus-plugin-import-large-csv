# Scribus plugin import large CSV

Scribus plugin : import large tables (CSV format) on several pages
Extension Scribus : import de grands tableaux (format CSV) qui peuvent tenir sur plusieurs pages

Ce script est une reprise du script initialement écrit par Gregory Pittman que l'on trouve dans Scribus. Encore sommaire, il gagnerait à avoir une vraie interface graphique faite avec TkInter (je sais, TkInter, c'est pas terrible, mais au moins c'est géré par Scribus).

Pour un mode d'emploi voir mon billet de blog [importer un tableau au format CSV](http://formation-logiciel-libre.com/scribus-importer-un-tableau-format-csv/).
